<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=ocmaintenance
ActiveAccessExtensions[]=occsvimport
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=ocinigui
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=occhangeobjectdate
ActiveAccessExtensions[]=jcremoteid
ActiveAccessExtensions[]=batchtool
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ezchangeclass
ActiveAccessExtensions[]=ezclasslists
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocextensionsorder
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=ocrss
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=openpa_bootstrapitalia
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocsupport
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=ezmbpaex
ActiveAccessExtensions[]=ocevents
ActiveAccessExtensions[]=ocopendata_forms
ActiveAccessExtensions[]=ocfoshttpcache
ActiveAccessExtensions[]=oceasyontology
ActiveAccessExtensions[]=occhart
ActiveAccessExtensions[]=cjw_newsletter
ActiveAccessExtensions[]=openpa_newsletter
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ocwebhookserver
ActiveAccessExtensions[]=eztagdescription

[SiteSettings]
LoginPage=embedded

[SiteAccessSettings]
RequireUserLogin=true
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=bootstrapitalia
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=ocbootstrap4
AdditionalSiteDesignList[]=ocbootstrap
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=ita-IT
ContentObjectLocale=ita-IT
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=ita-IT
TextTranslation=enabled

#################
##### DEBUG #####
#################

[ContentSettings]
ViewCaching=disabled

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled
AlwaysLog[]=warning
AlwaysLog[]=debug
AlwaysLog[]=notice
AlwaysLog[]=strict

[TemplateSettings]
DevelopmentMode=enabled
Debug=disabled
ShowXHTMLCode=enabled
TemplateCache=disabled
TemplateCompile=disabled
ShowUsedTemplates=enabled

[OverrideSettings]
Cache=disabled

[DesignSettings]
DesignLocationCache=disabled

*/ ?>
